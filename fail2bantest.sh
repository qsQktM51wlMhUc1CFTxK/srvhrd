#Fail2Ban; Install & configure
prog="fail2ban"

echo "Checking if Fail2Ban is installed"
if ! command -v $prog /dev/null
then
    echo $prog "could not be found"
    sudo apt install fail2ban -y
    exit
fi