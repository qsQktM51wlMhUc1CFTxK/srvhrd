#Check updates, install updgrades, enable automatic updates
chkUpdate="apt update"
insUpgrade="apt install dis-upgrade -y"

$chkUpdate && $insUpgrade

upDa="unattended-upgrades"
echo "Checking if Unattended Upgrades is installed"
if ! command -v $upDa /dev/null
then
    echo $upDa "could not be found, installing now."
    apt install unattended-upgrades
    sudo dpkg-reconfigure -plow unattended-upgrades
    exit
fi
